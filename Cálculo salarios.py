def CalculaImpuestos (nomina):
    try:
        nomina = int(nomina)
        Impuesto = 0.3*nomina
        return Impuesto
    except ValueError: 
        print('Introduzca un número por favor')

def Cálculo_del_coste_salarial (Impuesto,nomina):
    Coste_salarial = Impuesto + nomina 
    return Coste_salarial 

#Programa mejorado: varios empleados
lista_nombres = []
lista_salarios = []

numero = 8
while (numero != 0) :
    nombre = str(input('Introduce un nombre: '))
    lista_nombres.append(nombre)
    salario_nombre = int (input('Introduce su salario: '))
    lista_salarios.append(salario_nombre)

    diccionario_info = dict(zip(lista_nombres, lista_salarios))
    numero = int(input('Introduce 1 para continuar con el programa y 0 para terminar: '))

for key,value in diccionario_info.items():
    impuestos_empleado = CalculaImpuestos (value)
    coste_salarial_empleado = Cálculo_del_coste_salarial (impuestos_empleado, value)
    print('Impuestos para', key,': ',impuestos_empleado, '| Coste salarial', key, ': ', coste_salarial_empleado)

print (diccionario_info)

