import unittest
from empleado import Empleado
#Este código es para probar el código, pero más que una prueba parece un ejemplo. Estge código no es relevante. 
class TestEmpleado(unittest.TestCase):
#Python va a buscar si funcionan las pruebas unitarias

    def test_construir(self):
        el = Empleado('nombre', 5000)
        self.assertEqual(el.nomina,5000)

    def test_impuestos(self): #Probamos un caso muy simple: Cuando construyo un empleado, se corresponde a lo que debería ser. 
        el = Empleado('nombre', 5000)
        self.assertEqual(el.calculaimpuestos(),1500)

    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()
