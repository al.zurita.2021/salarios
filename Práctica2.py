#Práctica de la clase del viernes 18. Tema: Clases de objetos

#Funciones:
def Cálculo_del_coste_salarial (Impuesto,nomina):
    Coste_salarial = Impuesto + nomina 
    return Coste_salarial 


class Empleado: #Es algo parecido de función pero elevado de nivel. Reutilizo el concepto de empleado las veces que quiera

    def __init__(self,n,s): #Este es el constructor
        self.nombre = n
        self.nomina = s
#La función debe estar definida dentro de la clase, al estar dentro, pasa a ser un método
    def calculaimpuestos (self): #Yo como empleado se que tengo nombre e impuestos. 
        impuesto = self.nomina*0.3
        return impuesto
        
    def __str__ (self): #Método que imprime el empleado
        return 'El empleado {nombre} debe pagar {impuesto:.2f}'.format(nombre=self.nombre, impuesto = self.calculaimpuestos())

#Instancias: Las copias que hacemos de la clase
Pepe = Empleado('Pepe',20000) 
Ana = Empleado('Ana',30000)


import unittest
#Este código es para probar el código, pero más que una prueba parece un ejemplo. Estge código no es relevante. 
class TestEmpleado(unittest.TestCase):
#Python va a buscar si funcionan las pruebas unitarias

    def test_construir(self):
        el = Empleado('nombre', 5000)
        self.assertEqual(el.nomina,5000)

    def test_impuestos(self): #Probamos un caso muy simple: Cuando construyo un empleado, se corresponde a lo que debería ser. 
        el = Empleado('nombre', 5000)
        self.assertEqual(el.calculaimpuestos(),5)

    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

    def test_excption(self):
        self.assertRaises(Exception, Empleado("pepe",-100))

if __name__ == "__main__":
    unittest.main()


#Equivale a probarlo en programas diferentes. 


#Mi código tiene una plantilla (clase), 2 constructores (llamo dos veces a la clase), tiene 2 copias (instancias/objeto)
#No hace falta tener un entrada o salida. Si quiero hacer un buen empleado, no debería haber ningún print dentro de la clase. 
#Está mal visto usar el print porque no vamos a tener consola al usar la clase. 

#Con esta versión, nos da error el test de impuestos