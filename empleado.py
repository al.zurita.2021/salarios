class Empleado: #Es algo parecido de función pero elevado de nivel. Reutilizo el concepto de empleado las veces que quiera

    def __init__(self,n,s): #Este es el constructor
        self.nombre = n
        self.nomina = s
#La función debe estar definida dentro de la clase, al estar dentro, pasa a ser un método
    def calculaimpuestos (self): #Yo como empleado se que tengo nombre e impuestos. 
        impuesto = self.nomina*0.3
        return impuesto
        
    def __str__ (self): #Método que imprime el empleado
        return 'El empleado {nombre} debe pagar {impuesto:.2f}'.format(nombre=self.nombre, impuesto = self.calculaimpuestos())
